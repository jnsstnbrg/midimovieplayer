package se.prettygoodcoders;

import processing.core.PApplet;
import processing.video.Movie;
import se.prettygoodcoders.movie.MoviePlayer;

import java.util.ArrayList;

public class MovieMidiPlayer extends PApplet {

	private static final long serialVersionUID = 1L;

	private MusicEngine musicEngine;

	@Override
	public void init() {
		frame.removeNotify();
		frame.setUndecorated(true);
		frame.addNotify();

		japplemenubar.JAppleMenuBar jamb = new japplemenubar.JAppleMenuBar();
		jamb.setVisible(false, false);

		super.init();
	}

	@Override
	public void setup() {
		size(displayWidth, displayHeight);

		prepareExitHandler();
        frameRate(60);

		musicEngine = new MusicEngine(this);
		musicEngine.start();

		updateActiveMovie();
		startMovie();
	}

	@Override
	public void draw() {
		background(0);

		updateActiveMovie();
		musicEngine.getActiveMoviePlayer().updateDuration();
		musicEngine.getActiveMoviePlayer().render(this);
	}

	public void movieEvent(Movie m) {
		m.read();
	}

	public void startMovie() {
		musicEngine.getActiveMoviePlayer().play();
	}

	public void updateActiveMovie() {
		if (musicEngine.getActiveMoviePlayer() == null) {
			musicEngine.setActiveMoviePlayer(musicEngine.getMoviePlayerList().get(musicEngine.setIndexToZero()));
		} else {
			if (musicEngine.getActiveMoviePlayer().isFinished()) {
				if (musicEngine.getActiveIndex() + 1 == musicEngine.getMoviePlayerList().size()) {
					musicEngine.setActiveMoviePlayer(musicEngine.getMoviePlayerList().get(musicEngine.setIndexToZero()));
				} else {
					musicEngine.setActiveMoviePlayer(musicEngine.getMoviePlayerList().get(musicEngine.incrementIndex()));
				}
				rebootMovies();
				updateFinishedOnInactiveMovies(false);
			}
		}
	}

	public void updateFinishedOnInactiveMovies(boolean finished) {
		for (MoviePlayer moviePlayer : musicEngine.getMoviePlayerList()) {
			if (!moviePlayer.equals(musicEngine.getActiveMoviePlayer())) {
				moviePlayer.setFinished(finished);
			}
		}
	}

	public void rebootMovies() {
		for (MoviePlayer moviePlayer : musicEngine.getMoviePlayerList()) {
			if (!moviePlayer.equals(musicEngine.getActiveMoviePlayer())) {
				moviePlayer.stop();
			}
		}
		musicEngine.getActiveMoviePlayer().play();
	}

	private void prepareExitHandler() {
		try {
			Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {
				public void run() {
					musicEngine.stopAllNotes();
					System.out.println("Shutting down");
				}
			}));
		} catch (IllegalStateException e) {

		}
	}

	public static void main(String args[]) {
		ArrayList<String> argList = new ArrayList<String>();
		argList.add("--location=0,0");
		argList.add("--bgcolor=" + 000000);
		argList.add("--hide-stop");
		argList.add("se.prettygoodcoders.MovieMidiPlayer");
		PApplet.main(argList.toArray(new String[0]));
	}

}
