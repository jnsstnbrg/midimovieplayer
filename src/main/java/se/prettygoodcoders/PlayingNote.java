package se.prettygoodcoders;

/**
 * User: ivarboson
 */
public class PlayingNote {
    public int note;
    public int velocity;
    public int midiChannel;
    public int eigthTicksToLive;

    public PlayingNote(int midiChannel, int note, int velocity, int eigthTicksToLive){
        this.note = note;
        this.velocity = velocity;
        this.midiChannel = midiChannel;
        this.eigthTicksToLive = eigthTicksToLive;
    }

    public void tickDownLife(){
        this.eigthTicksToLive -= 1;
    }

    public boolean isTimeToStop(){
        return eigthTicksToLive <= 0;
    }
}
