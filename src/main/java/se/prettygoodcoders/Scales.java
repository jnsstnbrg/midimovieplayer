package se.prettygoodcoders;

import java.util.ArrayList;

/**
 * User: ivarboson
 */
public class Scales {

    public static final int MAJOR = 0;
    public static final int MINOR = 1;
    public static final int RANDOM = 2;
    private static int c = 60;
    private static int cs = 61;
    private static int d = 62;
    private static int ds = 63;
    private static int e = 64;
    private static int f = 65;
    private static int fs = 66;
    private static int g = 67;
    private static int gs = 68;
    private static int a = 69;
    private static int as = 70;
    private static int b = 71;


    public static int[] cmaj = new int[]{c, d, e, f, g, a, b};
    public static int[] cmin = new int[]{c, d, ds, f, g, a, as};
    public static int[] dmaj = new int[]{d, e, fs, g, a, b, cs+12};
    public static int[] dmin = new int[]{d, e, f, g, a, as, c+12};
    public static int[] emaj = new int[]{e, fs, gs, a, b+12, cs+12, ds+12};
    public static int[] emin = new int[]{e, fs, g, a , b, c+12, d+12};
    public static int[] gmaj = new int[]{g, a, b, c+12, d+12, e+12, fs+12};
    public static int[] amin = new int[]{a-12, b-12, c, d, e, f, g};
    public static int[] fmaj = new int[]{f-12, g-12, a-12, as-12, c, d, e};

    public static ArrayList<int[]> scales = new ArrayList<int[]>();
    public static ArrayList<int[]> majScales = new ArrayList<int[]>();
    public static ArrayList<int[]> minScales = new ArrayList<int[]>();

    public static ArrayList<int[]> getScales(){
        if(scales.size() > 0){
            return scales;
        }
        scales.add(cmaj);
        scales.add(gmaj);
        scales.add(amin);
        scales.add(fmaj);
        scales.add(cmin);
        scales.add(dmaj);
        scales.add(dmin);
        scales.add(emaj);
        scales.add(emin);
        return scales;
    }
    public static int[] getRandomScale(){
        getScales();
        int randomIndex = (int)(Math.random()*scales.size());
        return getScales().get(randomIndex);
    }

    public static int[] getScale(int type){
        if(minScales.size() == 0 || majScales.size() == 0){
            minScales.add(cmin);
            minScales.add(dmin);
            minScales.add(emin);
            minScales.add(amin);
            majScales.add(cmaj);
            majScales.add(dmaj);
            majScales.add(emaj);
            majScales.add(fmaj);
            majScales.add(gmaj);
        }

        switch(type){
            case MAJOR:
                return majScales.get(MusicEngine.random(0, majScales.size()));

            case MINOR:
                return minScales.get(MusicEngine.random(0, minScales.size()));

            case RANDOM:
                return getScales().get(MusicEngine.random(0, getScales().size()));

        }
        return scales.get(MusicEngine.random(0, scales.size()));
    }
}
