package se.prettygoodcoders;

import processing.core.PApplet;
import processing.event.KeyEvent;
import processing.video.Movie;
import se.prettygoodcoders.grid.Grid;
import se.prettygoodcoders.midibus.MidiBus;
import se.prettygoodcoders.movie.MovieCell;
import se.prettygoodcoders.movie.MoviePlayer;

import java.util.ArrayList;
import java.util.List;

/**
 * User: ivarboson
 */
public class MusicEngine implements Runnable {

	private MidiBus midiBus;

	private ArrayList<PlayingNote> notesToStart = new ArrayList<PlayingNote>();
	private ArrayList<PlayingNote> playingNotes = new ArrayList<PlayingNote>();

	private static final long timeChords = (60 * 1000 / 120) * 4;
	private static final long timeEights = (60 * 1000 / 120) / 2;
	private static final long timeFourths = (60 * 1000) / 120;
	private static final long timeChangeScale = 8000;

	private long chordsTimestamp;
	private long eightsTimestamp;
	private long fourthsTimestamp;
	private long scaleTimestamp;

	private int[] currentScale;

	private static int currentGridCell = 0;

	// Movie Stuffs
	public static final Grid Grid = new Grid(5, 2);
	private List<MoviePlayer> moviePlayerList;
	private MoviePlayer activeMoviePlayer;
	private int activeIndex;

	private Thread thread;

    private int eightsAdd = 0;

	private static boolean allowScaleChange = true;

	public MusicEngine(PApplet pApplet) {
		midiBus = new MidiBus(this, "Movie2MIDI");
		midiBus.addOutput("testOut");

		pApplet.registerMethod("keyEvent", this);

		chordsTimestamp = System.currentTimeMillis();
		eightsTimestamp = System.currentTimeMillis();
		fourthsTimestamp = System.currentTimeMillis();
		scaleTimestamp = System.currentTimeMillis();

		currentScale = Scales.cmaj;

		moviePlayerList = new ArrayList<MoviePlayer>();
		moviePlayerList.add(new MoviePlayer(pApplet, new Movie(pApplet, "theloneranger.mp4")));
		moviePlayerList.add(new MoviePlayer(pApplet, new Movie(pApplet, "startrek.avi")));
	}

	public void stopAllNotes() {
		for (int i = 0; i <= 127; i++) {
			for (int j = 0; j <= 5; j++) {
				midiBus.sendNoteOff(j, i, 0);
			}
		}
	}

	public MoviePlayer getActiveMoviePlayer() {
		return activeMoviePlayer;
	}

	public void setActiveMoviePlayer(MoviePlayer moviePlayer) {
		this.activeMoviePlayer = moviePlayer;
	}

	public List<MoviePlayer> getMoviePlayerList() {
		return moviePlayerList;
	}

	public int getActiveIndex() {
		return activeIndex;
	}

	public void setActiveIndex(int activeIndex) {
		this.activeIndex = activeIndex;
	}

	public int incrementIndex() {
		return ++activeIndex;
	}

	public int setIndexToZero() {
		activeIndex = 0;
		return activeIndex;
	}

	public void playSomeMusic() {
		if (System.currentTimeMillis() - eightsTimestamp >= timeEights) {
			System.out.println("play some music");
			stopNotes();
			switchScale();
			addChords();
			addFourths();
			addEight(currentGridCell);

			eightsTimestamp = System.currentTimeMillis();
		}
		if (notesToStart.size() > 0) {
			for (PlayingNote p : notesToStart) {
				playNote(p.midiChannel, p.note, p.velocity);
			}
			playingNotes.addAll(notesToStart);
			notesToStart.clear();
		}

	}

	public void stopNotes() {
		for (int i = playingNotes.size() - 1; i >= 0; i--) {
			PlayingNote p = playingNotes.get(i);
			p.tickDownLife();
			if (p.isTimeToStop()) {
				midiBus.sendNoteOff(p.midiChannel, p.note, p.velocity);
				playingNotes.remove(i);
			}
		}

	}

	public void switchScale() {
		if (allowScaleChange) {
			if (System.currentTimeMillis() - scaleTimestamp >= timeChangeScale) {
				currentScale = Scales.getScale(getActiveMoviePlayer().getMovieGrid().getAverageOnAll());
				System.out.println(">>>>>>>>>> switching scale!");
				scaleTimestamp = System.currentTimeMillis();
			}
		}
	}

	public void addFourths() {

		int count = 5 + (int) (Math.random() * 3);

		if (System.currentTimeMillis() - fourthsTimestamp >= timeFourths) {
			midiBus.sendNoteOn(5, 60, 127);
            if(Math.random() > 0.5){
                for (int i = 0; i < count; i++) {
                    if (Math.random() > 0.5) {

                        MovieCell cell = getActiveMoviePlayer().getMovieGrid().getMovieCellList().get(i);
                        synchronized (cell) {
                            if (cell != null && cell.getSound() != null) {
                                notesToStart.add(new PlayingNote(0, currentScale[(cell.getSound()[0]+(i*2)) % 6] + ((cell.getSound()[1]) * 12), 80, cell.getSound()[3]));
                            }
                        }
                    }
                }
            }
			currentGridCell++;
			if (currentGridCell >= Grid.xSize * Grid.ySize)
				currentGridCell = 0;
			fourthsTimestamp = System.currentTimeMillis();
			midiBus.sendNoteOff(5, 60, 127);
		}

	}

	public void addEight(int gridCell) {
		MovieCell cell = getActiveMoviePlayer().getMovieGrid().getMovieCellList().get(gridCell);
		synchronized (cell) {
			if (cell != null && cell.getSound() != null)    {
                if(eightsAdd > 0){
                    eightsAdd = 0;
				    notesToStart.add(new PlayingNote(1, currentScale[(cell.getSound()[0] + 5) % 6] + ((cell.getSound()[1]) * 12), cell.getSound()[2], cell.getSound()[3]));
                }else{
                    eightsAdd = 1;
                    notesToStart.add(new PlayingNote(1, currentScale[cell.getSound()[0] % 6] + ((cell.getSound()[1]) * 12), cell.getSound()[2], cell.getSound()[3]));
                }

            }
		}
	}

	public void addChords() {
		if (System.currentTimeMillis() - chordsTimestamp > timeChords) {

			notesToStart.add(new PlayingNote(2, currentScale[0] - 12, random(80, 100), 8));
			notesToStart.add(new PlayingNote(2, currentScale[2] - 12, random(80, 100), 8));
			notesToStart.add(new PlayingNote(2, currentScale[4] - 12, random(80, 100), 8));
			notesToStart.add(new PlayingNote(2, currentScale[4], random(80, 100), 8));
			notesToStart.add(new PlayingNote(2, currentScale[0], random(80, 100), 8));
			chordsTimestamp = System.currentTimeMillis();
		}
	}

	public void playNote(int channel, int note, int velocity) {
		System.out.println("playing note on channel " + channel);
		midiBus.sendNoteOn(channel, note, velocity);
	}

	public static int random(int offset, int maxVal) {
		return (int) (offset + (Math.random() * (maxVal - offset)));
	}

	public static void toggleAllowScaleChange() {
		allowScaleChange = !allowScaleChange;
	}

	void start() {
		if (thread == null) {
			thread = new Thread(this);
			thread.setName("se.prettygoodcoders.MusicEngine thread");
			thread.start();
		}
	}

	void stop() {
		if (thread != null) {
			thread.interrupt();
		}
		thread = null;
	}

	public void run() {
		Thread me = Thread.currentThread();
		while (thread == me) {
			playSomeMusic();
			try {
				Thread.sleep(10);
			} catch (InterruptedException e) {
			}
		}
		thread = null;
	}

	public void keyEvent(KeyEvent keyEvent) {
		switch (keyEvent.getAction()) {
		case KeyEvent.RELEASE:
			switch (keyEvent.getKey()) {
			case 'p':
			case 'P':
				if (activeMoviePlayer.isPlaying()) {
					activeMoviePlayer.pause();
				} else {
					activeMoviePlayer.play();
				}

				MusicEngine.toggleAllowScaleChange();
				break;
			}
			break;
		}
	}

	public static int getCurrentGridCell() {
		return currentGridCell;
	}
}
