package se.prettygoodcoders.movie;

import processing.core.PApplet;
import processing.core.PImage;
import se.prettygoodcoders.movie.PixelColor.Color;

import java.util.Arrays;
import java.util.Comparator;

public class MovieCell {

	private int x;
	private int y;

	private long rImage, gImage, bImage;

	private RGBColor previousRGBColor;
	private int[] sound;

	private RGBColor averageColor;

	public MovieCell(int x, int y) {
		this.x = x;
		this.y = y;
	}

	public void setSound(PImage pImage) {
		pImage.loadPixels();
		rImage = 0;
		gImage = 0;
		bImage = 0;

		for (int i = 0; i < pImage.pixels.length; i++) {
			int pixel = pImage.pixels[i];

			rImage += pixel >> 16 & 0xFF;
			gImage += pixel >> 8 & 0xFF;
			bImage += pixel & 0xFF;
		}

		averageColor = calculateAverage(pImage.pixels.length);

		// System.out.println(x + ":" + y + " - " + rgbColor.r + " " + rgbColor.g + " " + rgbColor.b);

		sound = calculateSound(averageColor);
	}

	private RGBColor calculateAverage(int numPixels) {
		rImage /= numPixels;
		gImage /= numPixels;
		bImage /= numPixels;

		return new RGBColor((int) rImage, (int) gImage, (int) bImage);
	}

	public RGBColor getAverageColor() {
		return averageColor;
	}

	public void setAverageColor(RGBColor averageColor) {
		this.averageColor = averageColor;
	}

	private int[] calculateSound(RGBColor rgbColor) {

		PixelColor[] pixelColorArray = new PixelColor[3];
		pixelColorArray[0] = new PixelColor(Color.RED, rgbColor.r);
		pixelColorArray[1] = new PixelColor(Color.GREEN, rgbColor.g);
		pixelColorArray[2] = new PixelColor(Color.BLUE, rgbColor.b);

		Arrays.sort(pixelColorArray, new Comparator<PixelColor>() {
			public int compare(PixelColor pixelColor1, PixelColor pixelColor2) {
				return Integer.valueOf(pixelColor2.getColorValue()).compareTo(Integer.valueOf(pixelColor1.getColorValue()));
			}
		});

		int octave = getOctave(pixelColorArray);
		int brightnessLevel = getBrightness(rgbColor);

		sound = new int[4];
		sound[0] = (int) PApplet.map(pixelColorArray[0].getColorValue(), 0, 180, 0, 6); // ton i skalan
		sound[1] = (int )PApplet.map(brightnessLevel, 0, 255, 0, 3); //octave; // oktaven
		sound[2] = (int) PApplet.map(pixelColorArray[1].getColorValue(), 0, 180, 0, 127); // velocity
		sound[3] = (int) PApplet.map(brightnessLevel, 0, 255, 0, 4); // l�ngd i ticks av 8-delar

		// System.out.println(x + ":" + y + " - " + sound[0] + " " + sound[1] + " " + differenceInRGB + " " + brightnessLevel);

		return sound;
	}

	private int getOctave(PixelColor[] colors) {
		// oktaven
		int octave = 0;
		switch (colors[0].getColor()) {
		case RED:
			if (colors[0].getColorValue() < 127) {
				octave = 0;
			} else {
				octave = 1;
			}
			break;
		case GREEN:
			if (colors[0].getColorValue() < 127) {
				octave = 1;
			} else {
				octave = 2;
			}
			break;
		case BLUE:
			if (colors[0].getColorValue() < 127) {
				octave = 2;
			} else {
				octave = 3;
			}
			break;
		default:
			throw new NullPointerException("Wrong color - Color missing in colors[]");
		}

		return octave;
	}

	private int getDifferenceInRGB(RGBColor rgbColor) {
		int differenceInRGBColor = 0;

		if (previousRGBColor != null) {
			int diffRed = Math.abs(rgbColor.r - previousRGBColor.r);
			int diffGreen = Math.abs(rgbColor.g - previousRGBColor.g);
			int diffBlue = Math.abs(rgbColor.b - previousRGBColor.b);

			float pctDiffRed = (float) diffRed / 255;
			float pctDiffGreen = (float) diffGreen / 255;
			float pctDiffBlue = (float) diffBlue / 255;

			differenceInRGBColor = (int) ((pctDiffRed + pctDiffGreen + pctDiffBlue) / 3 * 100);
		}

		previousRGBColor = rgbColor;

		return differenceInRGBColor;
	}

	private int getBrightness(RGBColor rgbColor) {
		return (rgbColor.r + rgbColor.g + rgbColor.b) / 3;
	}

	public int[] getSound() {
		return sound;
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

}
