package se.prettygoodcoders.movie;

import processing.core.PApplet;
import processing.video.Movie;
import se.prettygoodcoders.MusicEngine;

public class MoviePlayer {

	private MovieGrid movieGrid;

	private Movie movie;
	private boolean finished;
	private boolean playing;

	private int ratioWidth;
	private int ratioHeight;

	public MoviePlayer(PApplet pApplet, Movie movie) {
		this.movie = movie;

		movieGrid = new MovieGrid(125);
		movieGrid.start();
	}

	public void render(PApplet pApplet) {
		movieGrid.updateImage(movie);

		if (movie.width > 0 && movie.height > 0) {
			ratioWidth = pApplet.getWidth();
			ratioHeight = (int) (pApplet.getWidth() * ((float) movie.height / (float) movie.width));
		}

		pApplet.image(movie, 0, ratioHeight / 4, ratioWidth, ratioHeight);

		int newImageWidth = ratioWidth / MusicEngine.Grid.xSize;
		int newImageHeight = ratioHeight / MusicEngine.Grid.ySize;

		pApplet.noFill();
		pApplet.stroke(0);
		pApplet.strokeWeight(3);
		for (int i = 0; i < movieGrid.getMovieCellList().size(); i++) {
			MovieCell movieCell = movieGrid.getMovieCellList().get(i);
			if (MusicEngine.getCurrentGridCell() == i) {
				pApplet.fill(0, 127, 127, 100);
			} else {
				pApplet.noFill();
			}
			pApplet.rect(movieCell.getX() * newImageWidth, movieCell.getY() * newImageHeight + (ratioHeight / 4), newImageWidth, newImageHeight);
		}
	}

	public void updateDuration() {
		if (!finished && Math.abs(movie.time() - movie.duration()) < 0.01) {
			finished = true;
		}
	}

	public void play() {
		setPlaying(true);
		movie.play();
	}

	public void pause() {
		setPlaying(false);
		movie.pause();
	}

	public void stop() {
		setPlaying(false);
		movie.stop();
	}

	public boolean isFinished() {
		return finished;
	}

	public void setFinished(boolean finished) {
		this.finished = finished;
	}

	public Movie getMovie() {
		return movie;
	}

	public void setMovie(Movie movie) {
		this.movie = movie;
	}

	public MovieGrid getMovieGrid() {
		return movieGrid;
	}

	@Override
	public boolean equals(Object object) {
		if (object == null)
			return false;
		if (!(object instanceof PApplet))
			return false;

		MoviePlayer moviePlayer = (MoviePlayer) object;

		if (moviePlayer.getMovie().filename.equals(this.movie.filename))
			return true;

		return false;
	}

	public boolean isPlaying() {
		return playing;
	}

	public void setPlaying(boolean playing) {
		this.playing = playing;
	}

}
