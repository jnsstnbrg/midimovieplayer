package se.prettygoodcoders.movie;

import processing.core.PImage;
import se.prettygoodcoders.MusicEngine;
import se.prettygoodcoders.movie.PixelColor.Color;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

public class MovieGrid implements Runnable {

	private Thread t;
	private int sleepTime;

	private PImage pImage;

	private List<MovieCell> movieCellList;

	private int averageOnAll;

	public MovieGrid(int sleepTime) {
		this.sleepTime = sleepTime;

		movieCellList = new ArrayList<MovieCell>();
		populateMovieCells();
	}

	public void populateMovieCells() {
		for (int y = 0; y < MusicEngine.Grid.ySize; y++) {
			for (int x = 0; x < MusicEngine.Grid.xSize; x++) {
				movieCellList.add(new MovieCell(x, y));
			}
		}
	}

	public synchronized void updateImage(PImage pImage) {
		this.pImage = pImage;
	}

	public List<MovieCell> getMovieCellList() {
		return movieCellList;
	}

	public int getAverageOnAll() {
		return averageOnAll;
	}

	public void setAverageOnAll() {
		int rAverage = 0, gAverage = 0, bAverage = 0;
		for (MovieCell movieCell : movieCellList) {
			rAverage += movieCell.getAverageColor().r;
			gAverage += movieCell.getAverageColor().g;
			bAverage += movieCell.getAverageColor().b;
		}
		rAverage /= movieCellList.size();
		gAverage /= movieCellList.size();
		bAverage /= movieCellList.size();

		PixelColor[] pixelColorArray = new PixelColor[3];
		pixelColorArray[0] = new PixelColor(Color.RED, rAverage);
		pixelColorArray[1] = new PixelColor(Color.GREEN, gAverage);
		pixelColorArray[2] = new PixelColor(Color.BLUE, bAverage);

		Arrays.sort(pixelColorArray, new Comparator<PixelColor>() {
			public int compare(PixelColor pixelColor1, PixelColor pixelColor2) {
				return Integer.valueOf(pixelColor2.getColorValue()).compareTo(Integer.valueOf(pixelColor1.getColorValue()));
			}
		});

		switch (pixelColorArray[0].getColor()) {
		case RED:
			averageOnAll = 0;
			break;
		case GREEN:
			averageOnAll = 1;
			break;
		case BLUE:
			averageOnAll = 2;
			break;
		}
	}

	public void run() {
		Thread me = Thread.currentThread();
		while (t == me) {

			if (pImage != null) {
				synchronized (pImage) {
					int newImageWidth = pImage.width / MusicEngine.Grid.xSize;
					int newImageHeight = pImage.height / MusicEngine.Grid.ySize;

					for (MovieCell movieCell : movieCellList) {
						movieCell.setSound(pImage.get(newImageWidth * movieCell.getX(), newImageHeight * movieCell.getY(), newImageWidth, newImageHeight));
					}

					setAverageOnAll();
				}
			}

			try {
				Thread.sleep(sleepTime);
			} catch (InterruptedException e) {
			}
		}
		t = null;
	}

	void start() {
		if (t == null) {
			t = new Thread(this);
			t.setName("MovieGrid thread");
			t.start();
		}
	}

	void stop() {
		if (t != null) {
			t.interrupt();
		}
		t = null;
	}

	public void dispose() {
		stop();
	}

}
