package se.prettygoodcoders.movie;

public class PixelColor {

	public enum Color {
		RED, GREEN, BLUE;
	}

	private Color color;
	private int colorValue;

	public PixelColor(Color color, int colorValue) {
		this.color = color;
		this.colorValue = colorValue;
	}

	public Color getColor() {
		return color;
	}

	public void setColor(Color color) {
		this.color = color;
	}

	public int getColorValue() {
		return colorValue;
	}

	public void setColorValue(int colorValue) {
		this.colorValue = colorValue;
	}

	@Override
	public String toString() {
		return color + " " + colorValue;
	}

}
