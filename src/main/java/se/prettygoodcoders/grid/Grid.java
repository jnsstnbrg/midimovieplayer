package se.prettygoodcoders.grid;

public class Grid {

	public int xSize;
	public int ySize;

	public Grid(int xSize, int ySize) {
		this.xSize = xSize;
		this.ySize = ySize;
	}

}
